<?php

//Jualan Produk
//Komik
//Games

class Produk {
    public $judul,
            $penulis = "penulis",
            $penerbit = "penerbit",
            $harga = 0,
            $jmlhHalaman=0,
            $waktuMain=0,
            $tipe;

    //Constructor dapat diisi dengan nilai default && dapat mengAssign argument ke parameter dari instance of class Produk
    public function __construct($judul="judul", $penulis, $penerbit, $harga, $jmlhHalaman, $waktuMain, $tipe){
        $this->judul = $judul;
        $this->penulis = $penulis;
        $this->penerbit = $penerbit;
        $this->harga = $harga;
        $this->jmlhHalaman = $jmlhHalaman;
        $this->waktuMain = $waktuMain;
        $this->tipe = $tipe;
    }

    public function getLabel(){
        return  "$this->penulis, $this->penerbit";
    }

    public function getInfoLengkap(){
        $str = "{$this->tipe} : {$this->judul} | {$this->getLabel()} (Rp. {$this->harga})";
        ($this->tipe=="Komik")?($str.=" - {$this->jmlhHalaman} Halaman"):($str.=" - {$this->waktuMain} Jam");
        return $str;
    }
}


//object type
class CetakInfoProduk {
    //parameter $produk diambil dari instance of produk class berupa object
    public function cetak(Produk $produk){
        // var_dump($produk);
        $str = "{$produk->judul} | {$produk->getLabel()} (Rp. {$produk->harga})";
        return $str;
    }
}

// $produk1 = new Produk();
// //menimpa atau mengganti property
// $produk1->judul = "One Piece";
// var_dump($produk1);

// $produk2 = new Produk();
// $produk2->judul = "God Of war";
// //menambah property baru
// $produk2->tambahProperty = "Test";
// var_dump($produk2);


// $produk3 = new Produk();
// $produk3->judul ="Naruto";
// $produk3->penulis = "Mashashi Kishimoto";
// $produk3->penerbit = "Shonen Jump";
// $produk3->harga = 30000;

// $produk4 = new Produk();
// $produk4->judul = "Uncharted";
// $produk4->penulis = "Neil Druckman";
// $produk4->penerbit = "Sony Computer";
// $produk4->harga=250000;


//instance of produk class
$produk3 = new Produk("Naruto", "Mashashi Khisimoto", "Shonen Jump", 30000, 100, 0, "Komik");
// var_dump($produk3);
$produk4 = new Produk("Uncharted", "Neil Druckman", "Sony Computer", 250000, 0, 50, "Game");
echo "Komik : " . $produk3->getLabel();
echo "<br>";
echo "Game : " . $produk4->getLabel();
echo "<br>";

//object type
$infoproduk1 = new CetakInfoProduk();
//argument $produk1 adalah nilai berupa object yang akan di assign ke parameter $produk pada fungsi cetak yang berapa pada class CetakInfoProduk
echo $infoproduk1->cetak($produk3);
echo "<br>";
//produk1 berisi argument yang akan di assign ke __construct yang kemudian akan assgin ke property class produk
$infoproduk2 = new CetakInfoProduk();
echo $infoproduk2->cetak($produk4);
echo "<br>";
echo "<br>";


//inheritance problem
echo $produk3->getInfoLengkap();
echo "<br>";
echo $produk4->getInfoLengkap();