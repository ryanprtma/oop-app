<?php

//Jualan Produk
//Komik
//Games

class Produk {
    public $judul,
            $penulis = "penulis",
            $penerbit = "penerbit",
            $harga = 0;

    //Constructor dapat diisi dengan nilai default && dapat mengAssign argument ke parameter dari instance of class Produk
    public function __construct($judul="judul", $penulis, $penerbit, $harga){
        $this->judul = $judul;
        $this->penulis = $penulis;
        $this->penerbit = $penerbit;
        $this->harga = $harga;
    }

    public function getLabel(){
        return  "$this->penulis, $this->penerbit";
    }
}


//object type
class CetakInfoProduk {
    //parameter $produk diambil dari instance of produk class berupa object
    public function cetak(Produk $produk){
        $str = "{$produk->judul} | {$produk->getLabel()} (Rp. {$produk->harga})";
        return $str;
    }
}

// $produk1 = new Produk();
// //menimpa atau mengganti property
// $produk1->judul = "One Piece";
// var_dump($produk1);

// $produk2 = new Produk();
// $produk2->judul = "God Of war";
// //menambah property baru
// $produk2->tambahProperty = "Test";
// var_dump($produk2);


// $produk3 = new Produk();
// $produk3->judul ="Naruto";
// $produk3->penulis = "Mashashi Kishimoto";
// $produk3->penerbit = "Shonen Jump";
// $produk3->harga = 30000;

// $produk4 = new Produk();
// $produk4->judul = "Uncharted";
// $produk4->penulis = "Neil Druckman";
// $produk4->penerbit = "Sony Computer";
// $produk4->harga=250000;


//instance of produk class
$produk3 = new Produk("Naruto", "Mashashi Khisimoto", "Shonen Jump", 30000);

echo "Komik : " . $produk3->getLabel();
echo "<br>";
// echo "Game : " . $produk4->getLabel();

$infoproduk1 = new CetakInfoProduk();
//argument $produk1 adalah nilai berupa object yang akan di assign ke parameter $produk pada fungsi cetak
echo $infoproduk1->cetak($produk3);
//produk1 berisi argument yang akan di assign ke __construct yang kemudian akan assgin ke property class produk
