<?php

//Jualan Produk
//Komik
//Games

class Produk {
    public  $judul,
            $penulis = "penulis",
            $penerbit = "penerbit";

    protected $diskon;

    private $harga;
            

    //Constructor dapat diisi dengan nilai default && dapat mengAssign argument ke parameter dari instance of class Produk
    public function __construct($judul="judul", $penulis, $penerbit, $harga){
        $this->judul = $judul;
        $this->penulis = $penulis;
        $this->penerbit = $penerbit;
        $this->harga = $harga;
    }

    public function getLabel(){
        return  "$this->penulis, $this->penerbit";
    }

    public function getInfoProduk(){
        $str = "{$this->judul} | {$this->getLabel()} (Rp. {$this->harga})";
        return $str;
    }


    //method untuk menampikan harga dengan visibility property $harga private
    // public function getHarga(){
    //     return $this->harga;
    // }


    public function getHarga(){
        return $this->harga-($this->harga*$this->diskon/100);
    }
}

class Komik extends Produk {
    public $jmlhHalaman;

    public function __construct($judul="judul", $penulis="penulis", $penerbit="penerbit", $harga=0, $jmlhHalaman=0){
        parent::__construct($judul, $penulis, $penerbit, $harga);
        $this->jmlhHalaman=$jmlhHalaman;
    }


    public function getInfoProduk(){
        $str = "Komik :" . parent::getInfoProduk()." - {$this->jmlhHalaman} Halaman";
        return $str;
    }

    public function setDiskon($diskon){
        $this->diskon=$diskon;
    }
}

class Game extends Produk {
    public $waktuMain;
    public function __construct($judul="judul", $penulis="penulis", $penerbit="penerbit", $harga=0, $waktuMain=0){
        parent::__construct($judul, $penulis, $penerbit, $harga);
        $this->waktuMain=$waktuMain;
    }

    public function getInfoProduk(){
        $str = "Game : " . parent::getInfoProduk(). " - {$this->waktuMain} Jam";
        return $str;
    }

    public function setDiskon2($diskon){
        $this->diskon=$diskon;
    }
}

//object type
class CetakInfoProduk {
    //parameter $produk diambil dari instance of produk class berupa object
    public function cetak(Produk $produk){
        $str = "{$produk->judul} | {$produk->getLabel()} (Rp. {$produk->harga})";
        return $str;
    }
}

$produk3 = new Komik("Naruto", "Mashashi Khisimoto", "Shonen Jump", 30000, 100);
$produk4 = new Game("Uncharted", "Neil Druckman", "Sony Computer", 250000, 50);


echo $produk3->getInfoProduk();
echo "<br>";
echo $produk4->getInfoProduk();
echo "<br>";
$produk3->setDiskon(50);
echo $produk3->getHarga();
echo "<br>";
// $produk4->diskon=50;
echo "<br>";
// echo $produk4->getHarga();
$produk4->setDiskon2(50);
echo $produk4->getHarga();

